# Stakeholder Responses & Statements

## Table of Contents

- [Request For Approval Responses](#request-for-approval-responses)
  - [Nodes](#nodes)
  - [Wallets](#wallets)
  - [Projects](#projects)
  - [Industry](#industry)
- [Statements](#statements)
  - [Approve](#approve)
  - [Neutral](#neutral)
  - [Disapprove](#disapprove)

## Request For Approval Responses

The CHIP maintainer has contacted the following stakeholders for approval of this specification, using the following request template:

<details>

<summary><strong>Standard Approval Request Template</strong></summary>

<blockquote>

**Subject:** Nov 14 deadline: Adaptive Blocksize Limit Algorithm in 2024 BCH upgrade

Hello,

This is a formal request for approval from {{Organization}} regarding the May 2024 upgrade of Bitcoin Cash (BCH).

CHIP-2023-04 Adaptive Blocksize Limit Algorithm for Bitcoin Cash has achieved the Cash Improvement Proposal (CHIP) milestones required to be locked in on November 15, 2023 and activated on May 15, 2024.

Support for the Adaptive Blocksize Limit Algorithm upgrade is currently available in the following open-source node software:

- Bitcoin Cash Node (BCHN): https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node/-/merge_requests/1782

The CHIP contributors believe {{Organization}} is an important stakeholder in the Bitcoin Cash ecosystem.
**Please respond to the following question by 12:00 UTC on November 14, 2023. Non-responses will be considered "Abstain (Neutral)".**

> Does {{Organization}} approve of activating CHIP-2023-04 Adaptive Blocksize Limit Algorithm for Bitcoin Cash in the May 2024 Upgrade to Bitcoin Cash (BCH)?
> 
> - Yes (Approve)
> - No (Disapprove), or
> - Abstain (Neutral)
> 
> To make a public statement explaining this decision, please provide it here (required for disapproval):

For reference:

- CHIP-2023-04 Adaptive Blocksize Limit Algorithm for Bitcoin Cash: https://gitlab.com/0353F40E/ebaa
- Stakeholders & Responses: https://gitlab.com/0353F40E/ebaa/-/blob/main/stakeholders.md

Please let me know if you have any questions, and note that many common questions have been addressed in the CHIP:

- Could some adversary game the algorithm? https://gitlab.com/0353F40E/ebaa#spam-attack
- Could the algorithm be too fast? https://gitlab.com/0353F40E/ebaa#algorithm-too-fast
- Could the algorithm be too slow? https://gitlab.com/0353F40E/ebaa#algorithm-too-slow
- What about alternative X? https://gitlab.com/0353F40E/ebaa#evaluation-of-alternatives

Thank you,

bitcoincashautist  
Adaptive Blocksize Limit Algorithm CHIP maintainer  
Independent contributor (https://gitlab.com/A60AB5450353F40E)

</blockquote>

</details>

Responses have been logged by the maintainer and are presented below:

### Nodes

Responses from open source node implementations that support Bitcoin Cash.

| Node                                                    | As of Version | Approve | Disapprove | Neutral | Pending |
| ------------------------------------------------------- | :-----------: | :-----: | :--------: | :-----: | :-----: |
| [BCHD](https://bchd.cash/)                              |   `b6649f98`  |         |            |         |    ☐    |
| [Bitcoin Cash Node](https://bitcoincashnode.org/)       |   `b6649f98`  |    ☑    |            |         |         |
| [Bitcoin Unlimited](https://www.bitcoinunlimited.info/) |   `b6649f98`  |         |            |         |    ☐    |
| [Bitcoin Verde](https://bitcoinverde.org/)              |   `b6649f98`  |         |            |         |    ☐    |
| [Flowee](https://flowee.org/products/hub/)              |   `b6649f98`  |         |            |         |    ☐    |
| [Knuth](https://kth.cash/)                              |   `b6649f98`  |         |            |         |    ☐    |

### Wallets

Responses from wallets that support Bitcoin Cash.

| Wallet                                                           | As of Version | Approve | Disapprove | Neutral | Pending |
| ---------------------------------------------------------------- | :-----------: | :-----: | :--------: | :-----: | :-----: |
| [Bitcoin.com Wallet](https://wallet.bitcoin.com/)                |   `b6649f98`  |         |            |         |    ☐    |
| [BitPay Wallet](https://bitpay.com/wallet)                       |   `b6649f98`  |         |            |         |    ☐    |
| [Cashonize](https://cashonize.com/)                              |   `b6649f98`  |    ☑    |            |         |         |
| [Cashual Wallet](https://gitlab.com/monsterbitar/cashual-wallet) |   `b6649f98`  |    ☑    |            |         |         |
| [Cake Wallet](https://cakewallet.com/)                           |   `b6649f98`  |         |            |         |    ☐    |
| [Coinbase Wallet](https://www.coinbase.com/wallet)               |   `b6649f98`  |         |            |         |    ☐    |
| [Coin Wallet](https://coin.space/)                               |   `b6649f98`  |         |      ☒     |         |         |
| [Electron Cash](https://electroncash.org/)                       |   `b6649f98`  |         |            |    ○    |         |
| [Flowee Pay](https://flowee.org/products/pay/)                   |   `b6649f98`  |         |            |         |    ☐    |
| [Guarda](https://guarda.com/)                                    |   `b6649f98`  |         |            |         |    ☐    |
| [Ledger](https://www.ledger.com/)                                |   `b6649f98`  |         |            |         |    ☐    |
| [Melis](https://www.melis.io/)                                   |   `b6649f98`  |    ☑    |            |         |         |
| [Paytaca Wallet](https://www.paytaca.com/)                       |   `b6649f98`  |         |            |         |    ☐    |
| [Selene](https://selene.cash/)                                   |   `b6649f98`  |    ☑    |            |         |         |
| [Stack Wallet](https://stackwallet.com/)                         |   `b6649f98`  |         |            |         |    ☐    |
| [Trezor](https://trezor.io/)                                     |   `b6649f98`  |         |            |         |    ☐    |
| [Verde Wallet](https://github.com/SoftwareVerde/verde-wallet)    |   `b6649f98`  |         |            |         |    ☐    |
| [Zapit](https://www.zapit.io/)                                   |   `b6649f98`  |         |            |         |    ☐    |

### Projects

Responses from open source libraries, indexers, educational resources, and community initiatives.

| Project                                                           | As of Version | Approve | Disapprove | Neutral | Pending |
| ----------------------------------------------------------------- | :-----------: | :-----: | :--------: | :-----: | :-----: |
| [ActorForth](https://github.com/ActorForth/ActorForth)            |   `b6649f98`  |         |            |         |    ☐    |
| [Allodium](https://allodium.is/)                                  |   `b6649f98`  |    ☑    |            |         |         |
| [AnyHedge](https://anyhedge.com/)                                 |   `b6649f98`  |    ☑    |            |         |         |
| [awesomebitcoin.cash](https://awesomebitcoin.cash/)               |   `b6649f98`  |    ☑    |            |         |         |
| [bch.info](https://bch.info/)                                     |   `b6649f98`  |    ☑    |            |         |         |
| [`bch-rpc-explorer`](https://github.com/sickpig/bch-rpc-explorer) |   `b6649f98`  |    ☑    |            |         |         |
| [BCH DevSuite](https://devsuite.actorforth.org/)                  |   `b6649f98`  |         |            |         |    ☐    |
| [BCH Explorer](https://explorer.melroy.org/)                      |   `b6649f98`  |    ☑    |            |         |         |
| [BCH Guru](https://bch.guru/)                                     |   `b6649f98`  |         |            |         |    ☐    |
| [`bchtipbot`](https://github.com/merc1er/bchtipbot)               |   `b6649f98`  |         |            |         |    ☐    |
| [Bitauth IDE](https://ide.bitauth.com)                            |   `b6649f98`  |    ☑    |            |         |         |
| [BitcartCC](https://bitcartcc.com/)                               |   `b6649f98`  |         |            |         |    ☐    |
| [Bitcash](https://github.com/pybitcash/bitcash)                   |   `b6649f98`  |         |            |         |    ☐    |
| [Bitcoin Cash Argentina](https://bitcoincashargentina.com/)       |   `b6649f98`  |    ☑    |            |         |         |
| [Bitcoin Cash Foundation](https://bitcoincashfoundation.org/)     |   `b6649f98`  |    ☑    |            |         |         |
| [Bitcoin Cash Hangout](https://rss.com/podcasts/fiendishcrypto/)  |   `b6649f98`  |    ☑    |            |         |         |
| [Bitcoin Cash Podcast](https://bitcoincashpodcast.com/)           |   `b6649f98`  |    ☑    |            |         |         |
| [BitcoinCashResearch.org](https://bitcoincashresearch.org/)       |   `b6649f98`  |         |            |         |    ☐    |
| [BitcoinCashSite.com](https://bitcoincashsite.com/)               |   `b6649f98`  |         |            |         |    ☐    |
| [Bmap.app](https://bmap.app/)                                     |   `b6649f98`  |         |      ☒     |         |         |
| [CashAccount.info](https://www.cashaccount.info/)                 |   `b6649f98`  |    ☑    |            |         |         |
| [CashChannels.org](http://cashchannels.org/)                      |   `b6649f98`  |    ☑    |            |         |         |
| [CashFusion Red Team](https://fusionstats.redteam.cash/)          |   `b6649f98`  |         |            |         |    ☐    |
| [CashID.info](https://www.cashid.info/)                           |   `b6649f98`  |    ☑    |            |         |         |
| [CashNinja](https://bch.ninja/)                                   |   `b6649f98`  |    ☑    |            |         |         |
| [CashNinjas](https://ninjas.cash/)                                |   `b6649f98`  |         |            |         |    ☐    |
| [CashScript](https://cashscript.org/)                             |   `b6649f98`  |         |            |         |    ☐    |
| [CashTags](https://tags.infra.cash)                               |   `b6649f98`  |    ☑    |            |         |         |
| [CashTokens.org](https://cashtokens.org)                          |   `b6649f98`  |    ☑    |            |         |         |
| [Chaingraph](https://chaingraph.cash/)                            |   `b6649f98`  |    ☑    |            |         |         |
| [Developers.Cash](https://developers.cash/)                       |   `b6649f98`  |    ☑    |            |         |         |
| [Documentation.cash](https://documentation.cash/)                 |   `b6649f98`  |    ☑    |            |         |         |
| [Fex.cash](https://fex.cash/)                                     |   `b6649f98`  |    ☑    |            |         |         |
| [Flipstarter.cash](https://flipstarter.cash/)                     |   `b6649f98`  |    ☑    |            |         |         |
| [Flipstarter.me](https://flipstarter.me/)                         |   `b6649f98`  |    ☑    |            |         |         |
| [Flowee Products](https://flowee.org/products/)                   |   `b6649f98`  |         |            |         |    ☐    |
| [Fountainhead Cash](https://fountainhead.cash/)                   |   `b6649f98`  |         |            |         |    ☐    |
| [Fulcrum](https://github.com/cculianu/Fulcrum)                    |   `b6649f98`  |    ☑    |            |         |         |
| [Googol.cash Testnet Faucet](https://tbch4.googol.cash/)          |   `b6649f98`  |         |            |         |    ☐    |
| [Jedex](https://github.com/bitjson/jedex)                         |   `b6649f98`  |    ☑    |            |         |         |
| [Johoe's Mempool Statistics](https://jochen-hoenicke.de/queue/)   |   `b6649f98`  |         |            |         |    ☐    |
| [Knuth Libraries](https://github.com/k-nuth)                      |   `b6649f98`  |         |            |         |    ☐    |
| [Libauth](https://libauth.org/)                                   |   `b6649f98`  |    ☑    |            |         |         |
| [Mainnet.cash](https://mainnet.cash/)                             |   `b6649f98`  |    ☑    |            |         |         |
| [Oracles.cash](https://oracles.cash/)                             |   `b6649f98`  |    ☑    |            |         |         |
| [OpenTokenRegistry](https://otr.cash/)                            |   `b6649f98`  |    ☑    |            |         |         |
| [Permissionless Software Foundation](https://psfoundation.cash/)  |   `b6649f98`  |         |            |    ○    |         |
| [python-bitcoincash](https://pypi.org/project/bitcoincash/)       |   `b6649f98`  |    ☑    |            |         |         |
| [Rostrum](https://gitlab.com/bitcoinunlimited/rostrum)            |   `b6649f98`  |    ☑    |            |         |         |
| [rust-bitcoincash](https://crates.io/crates/bitcoincash)          |   `b6649f98`  |    ☑    |            |         |         |
| [ScanToPayCash](https://scantopay.cash)                           |   `b6649f98`  |    ☑    |            |         |         |
| [Watchtower](https://watchtower.cash/)                            |   `b6649f98`  |         |            |         |    ☐    |
| [Where2.cash](https://www.where2.cash/)                           |   `b6649f98`  |    ☑    |            |         |         |

### Industry

Responses from exchanges, miners, services, and other businesses.

| Organization                                                   | As of Version | Approve | Disapprove | Neutral | Pending |
| -------------------------------------------------------------- | :-----------: | :-----: | :--------: | :-----: | :-----: |
| [1BCH.com](https://1bch.com/)                                  |   `b6649f98`  |         |            |         |    ☐    |
| [3xpl](https://3xpl.com/)                                      |   `b6649f98`  |         |            |         |    ☐    |
| [Antpool](https://www.antpool.com/)                            |   `b6649f98`  |    ☑    |            |         |         |
| [Aptissio](https://www.aptissio.com/)                          |   `b6649f98`  |         |            |         |    ☐    |
| [BCH BULL](https://bchbull.com/)                               |   `b6649f98`  |    ☑    |            |         |         |
| [bch.domains](https://bch.domains/)                            |   `b6649f98`  |    ☑    |            |         |         |
| [BiggestLab.io](https://biggestlab.io)                         |   `b6649f98`  |         |            |         |    ☐    |
| [Bit.com](https://www.bit.com/)                                |   `b6649f98`  |         |            |         |    ☐    |
| [Bitauth.com](https://bitauth.com)                             |   `b6649f98`  |    ☑    |            |         |         |
| [Bitcoin Jason](https://jasonbch.com/)                         |   `b6649f98`  |    ☑    |            |         |         |
| [Bitcoin.com](https://bitcoin.com/)                            |   `b6649f98`  |         |            |         |    ☐    |
| [Bitfinex](https://www.bitfinex.com/)                          |   `b6649f98`  |         |            |         |    ☐    |
| [Bithumb](https://www.bithumb.com/)                            |   `b6649f98`  |         |            |         |    ☐    |
| [BitPay](https://bitpay.com/)                                  |   `b6649f98`  |         |            |         |    ☐    |
| [Bitstamp](https://www.bitstamp.net/)                          |   `b6649f98`  |         |            |         |         |
| [Bittrex](https://bittrex.com/)                                |   `b6649f98`  |         |            |         |         |
| [BitYard](https://www.bityard.com/)                            |   `b6649f98`  |         |            |         |    ☐    |
| [Blockchair](https://blockchair.com/)                          |   `b6649f98`  |         |            |         |    ☐    |
| [BTCPOP](https://btcpop.co/)                                   |   `b6649f98`  |         |            |         |    ☐    |
| [Cauldron](https://cauldron.quest)                             |   `b6649f98`  |    ☑    |            |         |         |
| [Clementine's Nightmare](https://clementinesnightmare.io/)     |   `b6649f98`  |         |            |         |    ☐    |
| [Coinbase](https://www.coinbase.com/)                          |   `b6649f98`  |         |            |         |         |
| [CryptoKnights.games](https://cryptoknights.games)             |   `b6649f98`  |         |            |         |    ☐    |
| [F2Pool](https://www.f2pool.com/)                              |   `b6649f98`  |         |            |         |         |
| [Foundry](https://foundrydigital.com/)                         |   `b6649f98`  |         |            |         |         |
| [FullStack.Cash](https://fullstack.cash/)                      |   `b6649f98`  |         |            |    ○    |         |
| [Gemini](https://www.gemini.com/)                              |   `b6649f98`  |         |            |         |    ☐    |
| [General Protocols](https://generalprotocols.com)              |   `b6649f98`  |    ☑    |            |         |         |
| [Kraken](https://kraken.com/)                                  |   `b6649f98`  |         |            |         |         |
| [KuCoin](https://www.kucoin.com/)                              |   `b6649f98`  |         |            |         |         |
| [Memo Technology, Inc.](https://memo.cash/)                    |   `b6649f98`  |         |            |         |    ☐    |
| [Mining-Dutch](https://www.mining-dutch.nl/)                   |   `b6649f98`  |         |            |         |         |
| [OKX](https://www.okx.com/)                                    |   `b6649f98`  |         |            |         |         |
| [Poolin](https://www.poolin.com/)                              |   `b6649f98`  |         |            |         |         |
| [Prohashing](https://prohashing.com/)                          |   `b6649f98`  |         |            |         |         |
| [Read.cash](https://read.cash/)                                |   `b6649f98`  |         |            |         |    ☐    |
| [Satoshi's Angels](https://www.satoshisangels.com/)            |   `b6649f98`  |         |            |         |    ☐    |
| [SBICrypto Pool](https://sbicrypto.com/)                       |   `b6649f98`  |         |            |         |         |
| [ShapeShift](https://shapeshift.com/)                          |   `b6649f98`  |         |            |         |         |
| [SideShift.ai](https://sideshift.ai/)                          |   `b6649f98`  |         |            |         |         |
| [Toomim Bros Bitcoin Mining Concern Ltd.](https://toom.im/)    |   `b6649f98`  |         |            |    ○    |         |
| [The Real Bitcoin Club](https://therealbitcoin.club/)          |   `b6649f98`  |         |      ☒     |         |         |
| [The Rock Trading](https://www.therocktrading.com/)            |   `b6649f98`  |         |            |         |         |
| [Trastra](https://trastra.com/)                                |   `b6649f98`  |         |            |         |         |
| [Travala](https://www.travala.com/)                            |   `b6649f98`  |         |            |         |         |
| [TxStreet](https://txstreet.com/)                              |   `b6649f98`  |         |            |         |    ☐    |
| [Upbit](https://www.upbit.com)                                 |   `b6649f98`  |         |            |         |    ☐    |
| [ViaBTC](https://www.viabtc.com/)                              |   `b6649f98`  |         |            |         |    ☐    |

## Statements

The following public statements have been submitted in response to this CHIP.

### Approve

The following articles have been published in support of this CHIP:

- [Jessquit](https://np.reddit.com/user/jessquit) (July 27, 2023): [Why Bitcoin Cash Needs an Adaptive Block Size Limit Algorithm](https://np.reddit.com/r/btc/comments/15azs0j/why_bitcoin_cash_needs_an_adaptive_block_size/)
- [Bitcoin Cash Podcast](https://bitcoincashpodcast.com/) (August 7, 2023): [Blocksize Algorithm CHIP endorsement](https://bitcoincashpodcast.com/blog/blocksize-algo-chip-endorsement)
- [General Protocols](https://generalprotocols.com) (September 9, 2023): [General Protocols supports CHIP 2023-04 Adaptive Blocksize Limit Algorithm for Bitcoin Cash](https://read.cash/@GeneralProtocols/general-protocols-supports-chip-2023-04-adaptive-blocksize-limit-algorithm-for-bitcoin-cash-49e2200f)
- [Bitjson](https://bitjson.com/) (November 3, 2023): [Bitcoin Cash Upgrade 2024](https://blog.bitjson.com/bitcoin-cash-upgrade-2024/)

The following statements have been submitted in support of this CHIP.

>The revisions to this CHIP have made it significantly easier to understand and clearly addresses all major concerns that I've seen raised in regards to this issue.  
>As a Fulcrum server operator, I endorse this proposal. I know I'd be able to keep up with operation costs even if we implemented BIP-101 instead, which would presently have us at 64mb block capacity.  
>I also recognize the urgency of solving this potential social attack before it becomes a problem again. I encourage adoption of this proposal for the November 2023 lock-in, allowing it to be activated in May 2024.

<p align="right">&mdash;
<a href="https://github.com/kzKallisti">kzKallisti</a>,
<a href="https://selene.cash">Selene</a>,
<a href="https://bch.ninja">CashNinja</a>.
</p>

>I am unable to comprehend the math, but I agree with and support the behaviour described and illustrated in various graphs. Further, I am impressed and content with the quality of research done in alternatives and simulating various edge cases and as such I am looking forward to activation of this improvement to managing our max blocksize limit.

<p align="right">&mdash;
<a href="https://gitlab.com/monsterbitar">Jonathan Silverblood</a>,
Bitcoin Cash developer.
</p>

>

>I (and The Bitcoin Cash Podcast & Selene Wallet) wholly endorse CHIP-2023-04 Adaptive Blocksize Limit Algorithm for Bitcoin Cash for lock-in 15 November 2023. A carefully selected algorithm that responds to real network demand is an obvious improvement to relieve social burden of discussion around optimal blocksizes plus implementation costs & uncertainty around scaling for miners & node operators. There is also some benefit to the community signalling its commitment to scaling & refusal to repeat the historic delays resulting from previous blocksize increase contention.  
>The amount of work done by bitcoincashautist has been very impressive & inspiring. I refer to not only work on the spec itself but also on iteration from feedback & communicating with stakeholders to patiently address concerns across a variety of mediums. Having reviewed the CHIP thoroughly, I am convinced the chosen parameters accomodate edge cases in a technically sustainable manner.  
>It is a matter of some urgency to lock in this CHIP for November. This will solidify the social contract to scale the BCH blocksize as demand justifies it, all the way to global reserve currency status. Furthermore, it will free up the community zeitgeist to tackle new problems for the 2025 upgrade.  
>A blocksize algorithm implementation is a great step forward for the community. I look forward to this CHIP locking-in in November & going live in May 2024!  

<p align="right">&mdash;
<a href="https://bitcoincashpodcast.com/about">Jeremy</a>,
<a href="https://bitcoincashpodcast.com/">The Bitcoin Cash Podcast</a>,
<a href="https://selene.cash">Selene</a>.
</p>

>Choosing a reasonable blocksize for a global p2p payment system has been an issue of debate in bitcoin for a decade. This CHIP proposal appears to settle the issue by providing a reasonable adjustable default for decades into into the future.  
>Both as research, and the much harder work of communication and exposition, BCA has developed an excellent proposal and presented it clearly to the community according to the accepted process.  
>If BCA's hard work is carried through with careful implementation and extensive testing by node developers, we as a community may look forward to never arguing about blocksize again.  
>' support the Adaptive Blocksize Limit Algorithm for Bitcoin Cash CHIP (8d6081cf) for the May 2024 upgrade.  
>IJHfkednCebJdIGva4Ol0o8qG02qBI3qu7ZNqCPO6mVxeXvKCKyxO0xkYW34s/qb1VKOd66zBt7LB6hPyRGollw=

<p align="right">&mdash;
<a href="https://github.com/2qx">2qx</a>,
Independent Bitcoin Application Developer,
<a href="https://unspent.app">Unspent Phi</a>,
<a href="https://github.com/mainnet-cash/mainnet-js">mainnet-js</a>,
<a href="https://awesomebitcoin.cash">awesomebitcoin.cash</a>,
<a href="https://github.com/2qx/tsugae">bitcoin tsugae</a>.
</p>

>In case it needs to be publicly declared. I personally support CHIP 2023-04 Adaptive Blocksize Limit Algorithm for Bitcoin Cash

<p align="right">&mdash;
<a href="https://twitter.com/CheapLightning">Cheapy</a>,
<a href="https://twitter.com/CheapLightning/status/1708042451032985975">Cat</a>.
</p>

>I fully support this CHIP, It took me quite a while to get through the many components within the chip and algorithm especially since my initial response was: but what if we have exponential adoption? Since if have some background (although limited) in control theory (mainly closed loop PID controllers) this piqued my interest in how the constants are determined in the algorithm. All I can say now is that the only reason not to implement this would be that it would be adding some complexity in a system where I personally admire base layer simplicity, that being said, bitcoincashautist has done an extraordinary job on covering my initial concerns and showing many examples of possible scenarios to the point i’m convinced the positive impact of this upgrade strongly outweighs any negatives I can think of. I really appreciate all the work that has been done on this and fully support it.

<p align="right">&mdash;
<a href="https://bitcoincashresearch.org/t/chip-2023-04-adaptive-blocksize-limit-algorithm-for-bitcoin-cash/1037/114">Kilian</a>,
mechatronics engineer with experience in robotics.
</p>

> I personally and on behalf of the projects and products I maintain and develop on Bitcoin Cash support CHIP 2023-04 Adaptive Blocksize Limit Algorithm for Bitcoin Cash.

<p align="right">&mdash;
<a href="https://twitter.com/dagur">dagurval</a>,
Bitcoin Cash developer,
</p>

>If that was not yet somehow clear enough from my previous comments, I hereby officially endorse this CHIP.

<p align="right">&mdash;
<a href="https://bitcoincashresearch.org/t/chip-2023-04-adaptive-blocksize-limit-algorithm-for-bitcoin-cash/1037/85?u=bitcoincashautist">ShadowOfHarbringer</a>,
/r/btc moderator
</p>

>Likewise, I formally endorse this CHIP.

<p align="right">&mdash;
<a href="https://bitcoincashresearch.org/t/chip-2023-04-adaptive-blocksize-limit-algorithm-for-bitcoin-cash/1037/88">Jessquit</a>,
/r/btc moderator
</p>

>I fully endorse CHIP 2023-04, & everything written by Jeremy perfectly sums up how I feel.
This is a real feather in BCH’s hat, & is a great signal that the community has long term plans for the network’s growth in a voluntary way.
>
>Here’s to BCH 2024 & beyond!

<p align="right">&mdash;
<a href="https://twitter.com/fiendishcrypto/status/1716195033425023218">Fiendish Crypto</a>,
/r/cashtokens moderator
</p>

>I cannot see any significant down-sides to this proposal:
>The maximum blocksize will never go below the current 32MB and BCA's excellent research/modeling should allow us to safely scale for future while avoiding social contention (or attacks) about an "appopriate max block size".
>The example code is also very nicely encapsulated, so I do not foresee this incurring any significant technical debt in future.
>Therefore, I endorse and support implementation of this CHIP for 2024 and thank BCA for the high degree of quality and thought put into it.

<p align="right">&mdash;
Jim Hamill,
<a href="https://developers.cash/">Developers.cash</a>,
<a href="https://tags.infra.cash">CashTags</a>,
<a href="https://scantopay.cash">ScanToPayCash</a>.
</p>

> This proposal resolves the <a href="https://blog.bitjson.com/bitcoin-cash-upgrade-2024/">economic vulnerability inherent to static block size limits</a> and could reasonably get us to universal Bitcoin Cash adoption without further intervention. Fourteen years of network usage data, across multiple ecosystems, provide the insight required to have confidence in deploying an adaptive block size algorithm.
> 
> On the specifics of the algorithm: I'm most focused on development of applications and services (primarily <a href="https://chaingraph.cash/">Chaingraph</a>, <a href="https://libauth.org/">Libauth</a>, and <a href="https://ide.bitauth.com/">Bitauth IDE</a>) where raising the block size limit imposes serious costs in development time, operating expense, and product capability. Even if hardware and software improvements technically enable higher limits, raising limits too far in advance of real usage forces entrepreneurs to wastefully redirect investment away from core products and user-facing development. By measuring observed usage, `CHIP-2023-04 Adaptive Blocksize Limit Algorithm` minimizes such waste while still ensuring adequate capacity for both bursts of activity and consistent future growth; I support this CHIP's activation in the May 2024 upgrade.

<p align="right">&mdash;
<a href="https://blog.bitjson.com/about">Jason Dreyzehner</a>,
<a href="https://ide.bitauth.com/">Bitauth IDE</a>,
<a href="https://chaingraph.cash/">Chaingraph</a>,
<a href="https://libauth.org/">Libauth</a>
</p>

### Disapprove

>I was ignoring this proposal as I believe it can never ever pass a public vote and I believed that bchautist would arrange that vote as Dreyzehmer did with cashtokens  
>...  
>Its time to create traffic, not another pointless blocksize debate that changes absolutely nothing and just drains energy and ressources from investors who pay devs

<p align="right">&mdash;
<a href="https://therealbitcoin.club/">realbitcoinclub</a>,
<a href="https://bmap.app/">Bmap.app</a>
<a href="https://bitcoincashresearch.org/t/chip-2023-04-adaptive-blocksize-limit-algorithm-for-bitcoin-cash/1037/95">(source)</a>
</p>

Maintainer response: https://bitcoincashresearch.org/t/1037/97

</br>

>Our position is – 32 MB is enough for BCH.

<p align="right">&mdash;
<a href="https://coin.space/">Coin Wallet</a>

Initial response was a plain "disapprove" without explanation and then maintainer asked for further clarifications and learned that the stakeholder is concerned about BSV-like inflation of the blocksize limit.
Maintainer then pointed out the fact that it would likely take multiple years to get to even 64 MB, and then asked to reconsider the "disapprove" position to which he got the above response.
To this we can only say: even with the algorithm activated, the limit will likely stay 32 MB (or very close to it) at least for a few years, as it would take sustained average block sizes of more than 10 MB to move it meaningfully away from 32 MB.

### Neutral
